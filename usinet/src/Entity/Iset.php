<?php

namespace App\Entity;

use App\Repository\IsetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IsetRepository::class)
 */
class Iset
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity=classe::class, mappedBy="iset", orphanRemoval=true)
     */
    private $classes_iset;

    public function __construct()
    {
        $this->classes_iset = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|classe[]
     */
    public function getClassesIset(): Collection
    {
        return $this->classes_iset;
    }

    public function addClassesIset(classe $classesIset): self
    {
        if (!$this->classes_iset->contains($classesIset)) {
            $this->classes_iset[] = $classesIset;
            $classesIset->setIset($this);
        }

        return $this;
    }

    public function removeClassesIset(classe $classesIset): self
    {
        if ($this->classes_iset->contains($classesIset)) {
            $this->classes_iset->removeElement($classesIset);
            // set the owning side to null (unless already changed)
            if ($classesIset->getIset() === $this) {
                $classesIset->setIset(null);
            }
        }

        return $this;
    }
}
